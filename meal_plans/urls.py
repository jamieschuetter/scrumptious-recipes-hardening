from django.urls import path

from meal_plans.views import (
    MealPlanListView,
    # MealPlanCreateView,
    # MealPlanDetailView,
    # MealPlanUpdateView,
    # MealPlanDeleteView,
)


from django.contrib.auth import views as auth_views

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    # path("<int:pk>/", MealPlanDetailView.as_view(),
    # / name="meal_plans_detail"),
    # path(
    # "<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plans_edit"
    # ),
    # path(
    # "<int:pk>/delete/",
    # MealPlanDeleteView.as_view(),
    # name="meal_plans_delete",
    # ),
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
]
