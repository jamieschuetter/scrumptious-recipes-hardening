from django import forms


from recipes.models import Rating


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


# class IngredientForm(forms.ModelForm):
# class Meta:
# model = Ingredient
# fields = ["character"]
